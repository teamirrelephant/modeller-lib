_ = require 'underscore'

DEFAULT_SEED = 514229
DEFAULT_A = 13
DEFAULT_M = 426389

exports.Random = (seed, a, m) ->
  seed = seed or DEFAULT_SEED
  a = a or DEFAULT_A
  m = m or DEFAULT_M  
  
  current = seed        
  next = ->
    current = a * current
    current = current % m
    current / m

  nextGauss = (sigma, expectation) ->
    rootOfTwo = 1.41
    sequence = for i in [0..5] then next()
    sumReduce = (acc, val) -> acc + val
    expectation + sigma * rootOfTwo * (_.reduce(sequence, sumReduce, 0) - 3)

  nextExponential = (lambda) ->
    (-1 / lambda) * Math.log(next())

  nextGamma = (lambda, gamma) ->
    reduceProduct = (acc, val) -> acc * val
    sequence = for i in [0..5] then next()
    product = _.reduce(sequence, reduceProduct, 1)
    (-1 / lambda) * Math.log(product)

  nextTriangle = (a, b) ->
    a + (b - a) * Math.max(next(), next())

  nextSimpson = (a, b) ->
    nextIn = (x, y) ->
      x + (y - x) * next()

    nextIn(a / 2, b / 2) + nextIn(a / 2, b / 2)

  return { next, nextGauss, nextExponential, nextGamma, nextTriangle, nextSimpson }
