fs = require 'fs'

exports.loadOptions = ->
  inputs = fs.readFileSync 'options.json'
  try
    return JSON.parse inputs.toString()
  catch error
    return { }
