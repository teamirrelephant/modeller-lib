lib = require './lib'
utils = require './utils'
_ = require 'underscore'
randomizer = require './randomizer'

options = utils.loadOptions()

elements = lib.elementTypes
generators = lib.generatorTypes
dropPolicies = lib.generatorDropPolicies
workers = lib.workerTypes
Element = lib.Element

eventFlow = []
currentEvent = null
prevEvent = null
timeBlocked = 0

taxiGeneratorState = -> currentEvent.target is 'tGenerator'
passangerGeneratorState = -> currentEvent.target is 'pGenerator'

passengerReady = null
taxiReady = null

taxiWorkerState = -> passengerReady?
passangerWorkerState = -> taxiReady?

tGen = Element(elements.generator)(generators.syncronized, dropPolicies.block)(taxiGeneratorState)
tQueue = Element(elements.queue)(options.q)
tWorker = Element(elements.worker)(workers.syncronized)(taxiWorkerState)

pGen = Element(elements.generator)(generators.syncronized, dropPoicies.drop)(passangerGeneratorState)
tWorker.on 'success', -> passengerReady = null
tWorker.on 'pass', -> (bid) -> taxiReady = bid

pQueue = Elements(elements.queue)(Number.MAX_VALUE)
pWorker = Element(elements.worker)(workers.syncronized)(passengerWorkerState)
pWorker.on 'success', -> taxiReady = null
pWorker.on 'pass', (bid) -> passengeReady = bid

endPassenger =
  tick: -> null
  pass: (e) -> null
  previous: (e) -> null
  
lib.link element: tGen, to: tQueue
lib.link element: tQueue, to: tWorker
lib.link element: tWorker, to: end

lib.link element: pGen, to: pQueue
lib.link element: pQueue, to: pWorker
lib.link element: pWorker, to: end

random = randomizer.Random();
generateDurationBasedOn = (factor) ->
  random.nextExponential(factor);
  
buildEventFlowForGenerator = ->
  eventFlow.unshift time: 0, target: 'generator'
  loop
    nextTime = eventFlow[eventFlow.length - 1].time + generateDurationBasedOn(options.lambda)
    if nextTime > options.simulateFor then break
    eventFlow.push time: nextTime , target: 'generator'
  eventFlow

worker.on 'successful-pass', (bid) ->
  insertSortingByTime = (target, item) ->
    targetIndex = target.indexOf _.find target, (arrayItem) -> arrayItem.time > item.time  
    target.splice targetIndex, 0, item
    
  event =
    time: currentEvent.time + generateDurationBasedOn(options.nu)
    target: 'worker'
  
  insertSortingByTime eventFlow, event

tickUntilEventFlowEnds = ->
  loop
    prevEvent = currentEvent
    currentEvent = eventFlow[0]
    if currentEvent.time > options.simulateFor then break
    eventFlow = eventFlow.slice(1)
    gen.tick()
    
buildEventFlowForGenerator()  
tickUntilEventFlowEnds()

console.log timeBlocked
