generatorTypes =
  randomized: 0
  determined: 1
  syncronized: 2

generatorDropPolicies =
  block: 0
  drop: 1

elementTypes =
  generator: 0
  worker: 1
  queue: 2

workerTypes =
  randomized: 0
  syncronized: 1

Bid = (tick) ->
  finished = '\"not yet\"'
  toString: () -> '#{Bid(@tick=' + tick + ';@finished=' + finished + ')}',
  finish: (at) -> finished = at
  finished: -> finished
  lifetime: -> if typeof (finished) is 'number' then finished - tick else 0
  
Element = (type) ->
  events = {}
  
  nextElements = []
  prevElement = null

  currentTick = 0

  findNextIdleElement = ->
    for element in nextElements
      return element if not element.state? or element.state().busy is false
    nextElements[0]
      
  after = (element) ->
    nextElements.push element

  previous = (element) ->
    prevElement = element
    
  passBack = (bid) ->
    trigger 'pass-back', bid

  attach  = (event, handler) ->
    events[event] ?= []
    events[event].push(handler)
  
  trigger = (event, args) ->
    handlers = events[event] || []
    for handler in handlers
      handler(args)

  runTick = (args) ->
    for e in nextElements then e.tick()
    trigger 'tick', ++currentTick
  
  BidGenerator = (type, dropPolicy) ->
    blockedBy = null

    passBack = (bid) ->
      if dropPolicy is generatorDropPolicies.block then blockedBy = bid
      trigger 'pass-back'
      
    DeterminedBidGenerator = (ttg) ->
      beforeBid = ttg

      tickActions = -> 
        if dropPolicy is generatorDropPolicies.block and blockedBy?
          next.pass blockedBy
          blockedBy = null       
        else if beforeBid is 0
          beforeBid--
          bid = Bid currentTick
          next = findNextIdleElement()
          next.pass bid
          beforeBid = ttg
          trigger 'new-bid', bid
    
      tick = ->
        runTick()
        tickActions()

      state = -> { ttg, beforeBid, blocked: blockedBy?, blockedBy: blockedBy }
      { tick, tickActions, passBack, after, previous, on: attach, state }
    
    RandomizedBidGenerator = (probability) ->
      state = -> { blocked: blockedBy?, blockedBy: blockedBy }

      tick = () ->
        runTick()

        next = findNextIdleElement()
        if dropPolicy is generatorDropPolicies.block and blockedBy?
          next.pass blockedBy
          blockedBy = null
        if Math.random() < probability
          bid = Bid currentTick
          next.pass bid
          trigger 'new-bid', bid
      
      { tick, state, passBack, after, previous, on: attach }

    SyncronizedBidGenerator = (parserFunc) ->
      state = -> { blocked: blockedBy?, blockedBy: blockedBy }
            
      tick = ->
        runTick()

        next = findNextIdleElement()
        if dropPolicy is generatorDropPolicies.block and blockedBy?
          next.pass blockedBy
          blockedBy = null
        else if parserFunc()
          bid = Bid currentTick
          next.pass bid
          trigger 'new-bid', bid
        
      { tick, passBack, after, previous, on: attach, state }
                
    if type is generatorTypes.determined
      DeterminedBidGenerator
    else if type is generatorTypes.randomized
      RandomizedBidGenerator
    else if type is generatorTypes.syncronized
      SyncronizedBidGenerator
    else
      null

  Worker = (type) ->
    currentTick = 0
    currentBid = null

    pass = (bid) ->
      if currentBid
        if prevElement then prevElement.passBack(bid)
        trigger 'overflow', current: currentBid, passed: bid
      else
        currentBid = bid
        trigger 'successful-pass', bid    

      trigger 'pass', bid

    state = ->
      busy: currentBid isnt null
      current: currentBid 

    RandomizedWorker = (probability) ->
      tick = ->
        runTick()

        if !currentBid
          trigger 'empty', null
          return;
         
        if Math.random() < probability
          if currentBid
            passed = currentBid
            currentBid = null
            next = findNextIdleElement()
            next.pass passed
            trigger 'success', passed
        else
          trigger 'fail', currentBid
    
      { tick, passBack, pass, after, previous, on: attach, state }

    SyncronizedWorker = (stateFunc) ->
      tick = ->
        runTick()

        if !currentBid
          trigger 'empty', null
          return;
                
        if stateFunc()
            passed = currentBid
            currentBid = null
            next = findNextIdleElement()
            next.pass passed
            trigger 'success', passed
        else
          trigger 'fail', currentBid
    
      { tick, passBack, pass, after, previous, on: attach, state }

    if type is workerTypes.randomized
      RandomizedWorker
    else if type is workerTypes.syncronized
      SyncronizedWorker
    else
      null

  Queue = (size) ->
    queue = []

    tickActions = ->
      if queue.length isnt 0
        bid = queue.pop() 
        next = findNextIdleElement()
        next.pass bid
        trigger 'propagate', bid
    
    tick = () ->
      runTick()
      tickActions()

    pass = (bid) ->
      if queue.length >= size
        prevElement.passBack bid
        trigger 'overflow', bid
      else if queue.length is 0
        next = findNextIdleElement()
        next.pass bid
        trigger 'pass-through', bid
      else
        queue.unshift bid
        trigger 'queue-up', bid: bid, size: queue.length

      trigger 'pass', bid
      
    passBack = (bid) -> 
      queue.push bid
      trigger 'queue-up', bid: bid, size: queue.length

    state = ->
      busy: queue.length is size 
      elements: queue
      length: queue.length
      size: size
    
    { tick, passBack, pass, after, previous, on: attach, state }
  
  if type is elementTypes.generator
    BidGenerator
  else if type is elementTypes.worker
    Worker
  else if type is elementTypes.queue
    Queue
  else
    null
    
exports.Bid = Bid
exports.Element = Element
exports.generatorTypes = generatorTypes
exports.elementTypes = elementTypes
exports.workerTypes = workerTypes
exports.generatorDropPolicies = generatorDropPolicies

exports.link = (items) ->
  items.element.after items.to
  items.to.previous items.element
