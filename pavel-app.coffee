lib = require './lib'
utils = require './utils'
_ = require 'underscore'
randomizer = require './randomizer'

options = utils.loadOptions()

elements = lib.elementTypes
generators = lib.generatorTypes
dropPolicies = lib.generatorDropPolicies
workers = lib.workerTypes
Element = lib.Element

eventFlow = []
currentEvent = null
prevEvent = null

droppedGenerator = 0
droppedWorker = 0
succeeded = 0

generatorState = -> currentEvent.target is 'generator'
workerState = -> currentEvent.target is 'worker'
workerState2 = -> currentEvent.target is 'worker2'

gen = Element(elements.generator)(generators.syncronized, dropPolicies.drop)(generatorState)
queue = Element(elements.queue)(options.q)
worker = Element(elements.worker)(workers.syncronized)(workerState)
queue2 = Element(elements.queue)(options.q)
worker2 = Element(elements.worker)(workers.syncronized)(workerState2)

end =
  tick: -> null
  pass: (e) -> null#succeeded++
  previous: (e) -> null
  
lib.link element: gen, to: queue
lib.link element: queue, to: worker
lib.link element: worker, to: queue2
lib.link element: queue2, to: worker2
lib.link element: worker2, to: end

random = randomizer.Random();
generateDurationBasedOn = (factor) ->
  random.nextExponential(factor);
  
buildEventFlowForGenerator = ->
  eventFlow.unshift time: 0, target: 'generator'
  loop
    nextTime = eventFlow[eventFlow.length - 1].time + generateDurationBasedOn(options.lambda)
    if nextTime > options.simulateFor then break
    eventFlow.push time: nextTime , target: 'generator'
  eventFlow

worker.on 'successful-pass', (bid) ->
  insertSortingByTime = (target, item) ->
    targetIndex = target.indexOf _.find target, (arrayItem) -> arrayItem.time > item.time  
    target.splice targetIndex, 0, item
    
  event =
    time: currentEvent.time + generateDurationBasedOn(options.nu)
    target: 'worker'
  
  insertSortingByTime eventFlow, event

worker2.on 'successful-pass', (bid) ->
  insertSortingByTime = (target, item) ->
    targetIndex = target.indexOf _.find target, (arrayItem) -> arrayItem.time > item.time  
    target.splice targetIndex, 0, item
    
  event =
    time: currentEvent.time + generateDurationBasedOn(options.nu)
    target: 'worker2'
  
  insertSortingByTime eventFlow, event

gen.on 'pass-back', ->
  droppedGenerator++

worker.on 'pass-back', ->
  droppedWorker++

tickUntilEventFlowEnds = ->
  loop
    prevEvent = currentEvent
    currentEvent = eventFlow[0]
    if currentEvent.time > options.simulateFor then break
    eventFlow = eventFlow.slice(1)
    gen.tick()

gen.on 'new-bid', ->
  succeeded++

console.log 'building event flow for generator...'
buildEventFlowForGenerator()
console.log 'working...'
tickUntilEventFlowEnds()

console.log succeeded

dgr = (droppedGenerator / succeeded)
dwr = (droppedWorker / succeeded)

console.log 'dropped @ generator: ' + dgr
console.log 'dropped @ worker: ' + dwr
console.log 'total: ' + (dgr + dwr)
